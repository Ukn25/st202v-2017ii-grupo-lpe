//============================================================================
// Name        : binario.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdlib.h>
#include<string.h>
#include<time.h>
#include<cstring>
#include <windows.h>
#define MAXT 1001
#define MAXP 1000

using namespace std;

void ListaDeNumeros();
void Burbuja();
int BusquedaBinaria(int Arreglo[],int cantidad, int num);

char        texto[MAXT] ;
char        patron[MAXP] ;
signed int  rep  ;            // numero de veces encontrado
signed int  comp ;            // numero de comparaciones

void preBMBc(char *P, int m, int tabla[]);
void BMH( char *T, int n , char *P, int m) ;

void Ordenar(int v[], int a);

void funcion1();
void funcion2();
void funcion3();
void funcion4();


int Arr[100];
int n;
int cant;

int main(){
	int opcion;
	cout<<"Que algoritmo desea usar?"<<endl;
	cout<<"*************************"<<endl;
	cout<<"(1) Busqueda binaria"<<endl;
	cout<<"(2) Algoritmo de BayerMoore"<<endl;
	cout<<"(3) Algoritmo de La Burbuja Bidireccional"<<endl;
	cout<<"(4) Algoritmo de Fibonacci"<<endl<<endl;

	cout<<"Ingrese el numero del algoritmo que desea probar ";
	cin>>opcion;

	switch (opcion){
	case 1 : funcion1();break;
	case 2 : funcion2();break;
	case 3 : funcion3();break;
	//case 4 : funcion2();break;

	}
	return 0;
}

void funcion1(){
	ListaDeNumeros();
    Burbuja();
    cout<<endl;
    cout<<"ingrese el numero que desea buscar : ";
    cin>>n;
    if(BusquedaBinaria(Arr,cant,n)>=0){
        cout<<"El numero : "<<n<<" esta en el arreglo ordenado esta en la posicion "<<BusquedaBinaria(Arr,cant,n)<<endl;
    }
    else{
        cout<<"El numero : "<<n<<" no se encuentra en el arreglo"<<endl;
    }
}


 void ListaDeNumeros(){
     cout<<endl;
     cout<<"Esta secci�n es para construir el arreglo"<<endl;
     cout<<"*****************************************"<<endl<<endl;
    do{
        cout<<"Ingrese el numero de elementos para su arreglo arreglo : ";
        cin>>cant;
    }while(cant<0 || cant>100);
    for(int i=0;i<cant;i++){
        cout<<"El termino "<<i<<" es :   ";
        cin>>Arr[i];
    }
 }

 void Burbuja(){
    int aux;

    for(int i=0;i<cant;i++){
        for(int j=0;j<cant-i-1;j++){
            if (Arr[j]>Arr[j+1]){
                aux=Arr[j];
                Arr[j]=Arr[j+1];
                Arr[j+1]=aux;
            }
        }
    }
    cout <<"El arreglo ordenado es :"<<endl;
    for(int i=0;i<cant;i++){
        cout<<"   "<<Arr[i]<<"   ";
    }

}

 int BusquedaBinaria(int Arreglo[],int cantidad,int num){

 int inf=0,sup,mitad;
 char b='F';
 sup=cantidad;
 while(inf<=sup){

    mitad=(inf+sup)/2;

        if(Arreglo[mitad]==num){
            b='V';
            break;
        }
        if(Arreglo[mitad]>num){
            sup=mitad;
            mitad=(inf+sup)/2;
        }
        if(mitad<num){
            inf=mitad;
            mitad= (inf+sup)/2;
        }
 }
//pocision mitad

    return mitad;

}

 void funcion2(){

     float t1 , t2, tiempo ;  // variables pata tiempo de busqueda
     rep = 0 ;
     cout<<endl ;
     cout<<"\t �����������������������������������������������������ͻ "<<endl;
     cout<<"\t � ALGORITMO BOOYER M. HORSPOOL � "<< endl ;
     cout<<"\t � ---------------------------------------------- � "<< endl ;
     cout<<"\t �����������������������������������������������������ͼ "<<endl<<endl;

     cout<<endl<<" Ingrese Texto :" ;
     cin.getline(texto,1001,'\n') ;
     cout<<endl;

     cout<<endl<<" Ingrese Patron :" ;
     cin.getline(patron,1000,'\n');

     int n  = strlen( texto ) ;
     int m  = strlen( patron ) ;

     cout<<"\n__________________________________________________________"<<endl<<endl;

     t1 = clock();
     BMH( texto , n , patron , m ) ;
     t2 = clock();

     cout<<"\n__________________________________________________________"<<endl<<endl;

     tiempo = (t2-t1)/100000 ;
     cout<<endl<<" >> Tiempo de busqueda : "<< tiempo ;

     if(rep  == 0)
         cout<<endl<<endl<<" >> Patron no encontrado ..! " ;
     else
         cout<<endl<<endl<<" >> Ocurrencias : "<< rep ;

     cout<<endl<<endl<<" >> Comparaciones : "<< comp ;

     cout<<"\n\n__________________________________________________________"<<endl<<endl;

 }
 void preBMBc(char *P, int m, int tabla[]){
     int i;
     for (i = 0; i < 256; ++i)
         tabla[i] = m;

     for (i = 0; i < m - 1; ++i)
     {
         tabla[P[i]] = m - i - 1;
     }
 }

 void BMH( char *T, int n , char *P, int m)
 {
     int  j , bmBC[256] ;
     char c ;

     preBMBc(P, m, bmBC) ;  // Preprocesamiento

     // B�squeda

     j = 0;
     while (j <= n - m)
     {
         c = T[j + m - 1] ;

         if ( P[m - 1] == c && memcmp(P, T + j, m - 1) == 0 )
         {
              cout<<" * Encontrado en : "<< j + 1 << endl;
              rep ++ ;
         }

         j = j + bmBC[c] ;     comp ++ ;

     }
 }

 void funcion3(){
     int a;
 	cout<<"Ingresa el numero de numeros que va a ordenar: "<<endl;
 	cin>>a;
 	int x[a];
     cout<<"Ingrese los numeros: "<<endl;
     for (int i=0;i<a;i++){
     	cin>>x[i];
     }
     Ordenar(x,a);
 }
 void Ordenar(int x[],int  a){
 	int i,j,c=0;
 	int m = a;
     do{
         for (i=0; i<m-1; i++){
             if (x[i] > x[i + 1]){
                 x[i] = x[i] + x[i + 1];
                 x[i + 1] = x[i] - x[i + 1];
                 x[i] = x[i] - x[i + 1];
 			}
  		}
         m = m - 1;
         for (j= a - 1,c=0; j >= c; j--){
             if(x[j] < x[j - 1]){
                 x[j] = x[j] + x[j - 1];
                 x[j - 1] = x[j] - x[j - 1];
                 x[j] = x[j] - x[j - 1];
             }
         }
         c = c + 1;
         cout<<"El ordenamiento no "<<a-i<<" es: ";
         for(int i=0;i<a;i++){
             	cout<<x[i]<<" ";
             }
         cout<<endl;
        // _sleep (3000);
  	}while (m != 0 && c != 0);
     cout<<"Los elementos ordenados son: "<<endl;
     for (i = 0; i < a; i++){
         cout<<" "<<x[i]<<", ";
     }
 }

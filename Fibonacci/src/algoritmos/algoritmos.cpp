/*
 * funciones.cpp
 *
 *  Created on: 30 sept. 2017
 *      Author: Pc
 */

#include "../algoritmos/algoritmos.h"

#include "../utilitarios/utilitarios.h"


int FibonacciBusqueda(int vector[],int tamanio_vector,int elementobuscado)
	{//Buscando el menor n�mero de Fibonacci mayor o igual a tamanio_vector
	int fibMm1=1,fibMm2=0;
	int fibM=fibMm1 + fibMm2;
	int posicion_rango=0;
	while(fibM<tamanio_vector)
		{fibMm2=fibMm1;
		 fibMm1=fibM;
		 fibM=fibMm1 + fibMm2;
		}
	//Con fibM>1 se verifica que fibMm2>0
	while(fibM>1){
		//Se verififca si el indice que se escoge est� dentro del vector
		int i= min(fibMm2+posicion_rango,tamanio_vector);
		if(elementobuscado>vector[i])
			{
				fibM=fibMm1;
			 	fibMm1=fibMm2;
			 	fibMm2=fibM-fibMm1;
			 	posicion_rango=i;
			}
		//El elemento buscado es menor
		else if(elementobuscado<vector[i])
			{
				fibM=fibMm2;
				fibMm1-=fibMm2;
				fibMm2=fibM-fibMm1;
			}
		else {//Para una indexaci�n que comienza con 1
				return 1+i;
			}

				 }
	//Se compara el ultimo elemento restante con elementobuscado
	if (vector[0]==elementobuscado){return 1;}
	//No se encontro el elemento en el vector
	return -1;
	};

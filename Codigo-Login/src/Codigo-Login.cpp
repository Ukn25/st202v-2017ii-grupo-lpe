//============================================================================
// Name        : Codigo-Login.cpp
// Author      : Diaz Vargas Wilson Albert
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <windows.h>
#include <conio.h>
#include <string.h>
#include <string>

using namespace std;

void clean();
void fin();
void clean();
int ExisteArchivo(char* filename);

int main(){
	int menu;
	cout << "FARMACIA. DEL GRUPO 4..." << endl;
	cout << "----------------------------" << endl;
	cout << "1. Ingrese como administrador"<<endl;
	cout << "2. Ingresar como Usuario" << endl;
	cout << "3. Registrarse como Usuario" << endl;
	cout << "5. Salir del Programa." << endl;
	cout << "----------------------------" <<endl<<endl<<endl;
	cout << "Introduzca Opcion:  ";
	cin >> menu;
	if (menu==5){
		fin();
	}
	if((menu<=0) or (menu>4)){
		cout<<endl;
		cout<<" Esta opcion no esta en la lista, el programa volvera al menu principal en 3 segundos, ";
		Sleep(1000);
		cout<<"2 segundos";
		Sleep(1000);
		cout<<", 1 segundo...";
		Sleep(1000);
		clean();
		main();
	}
	switch(menu){
		case 1:{
			string admi, pass;
			cout<<"Nick del administrador: ";
			cin>>admi;
			if(admi=="albert"){
				cout<<"Contrasenia: ";
				cin>>pass;
				if(pass=="albert123"){
					cout<<"Bienvenido administrador";
				}
				else{
					cout<<endl<<endl;
					cout<<"          Contrasenia incorrecta, intente de nuevo."<<endl<<endl;
					Sleep(3000);
					cout<<"Contrasenia: ";
					cin>>pass;
					if(pass=="albert123"){
						cout<<"Bienvenido administrador";
					}
					else{
						cout<<"Contrasenia incorrecta, el programa volvera al menu principal.";
						clean();
						main();
						return 0;
					}
				}
			}
			if(admi=="daniel"){
				cout<<"Contrasenia: ";
				cin>>pass;
				if(pass=="daniel123"){
					cout<<"Bienvenido administrador";
				}
				else{
					cout<<endl<<endl;
					cout<<"          Contrasenia incorrecta, intente de nuevo."<<endl<<endl;
					Sleep(3000);
					cout<<"Contrasenia: ";
					cin>>pass;
					if(pass=="daniel123"){
						cout<<"Bienvenido administrador";
					}
					else{
						cout<<"Contrasenia incorrecta, el programa volvera al menu principal";
						clean();
						main();
					}
				}
			}
			if(admi=="david"){
				cout<<"Contrasenia: ";
				cin>>pass;
				if(pass=="david123"){
					cout<<"Bienvenido administrador";
				}
				else{
					cout<<endl<<endl;
					cout<<"          Contrasenia incorrecta, intente de nuevo."<<endl<<endl;
					Sleep(3000);
					cout<<"Contrasenia: ";
					cin>>pass;
					if(pass=="david123"){
						cout<<"Bienvenido administrador";
					}
					else{
						cout<<"Contrasenia incorrecta, el programa se cerrar�";
						return 0;
					}
				}
			}
			if(admi=="percy"){
				cout<<"Contrasenia: ";
				cin>>pass;
				if(pass=="percy123"){
					cout<<"Bienvenido administrador";
				}
				else{
					cout<<endl<<endl;
					cout<<"          Contrasenia incorrecta, intente de nuevo."<<endl<<endl;
					Sleep(3000);
					cout<<"Contrasenia: ";
					cin>>pass;
					if(pass=="percy123"){
						cout<<"Bienvenido administrador";
					}
					else{
						cout<<"Contrasenia incorrecta, el programa volver�a al menu principal";
						clean();
						main();
						return 0;
					}
				}
			}
			else{
				cout<<"Nombre de administrador incorrecto, volvera al menu principal en 2 segundos.";
				Sleep(1000);
				cout<<"1 segundo";
				Sleep(2000);
				clean();
				main();
			}
		}
		case 2:{
			int h;
			cout<<"   Ingrese el numero de caracteres que tiene su nombre de usuario: ";
			cin>>h;
			char usuario[h+3];
			string con;
			ofstream archivo;
			cout<<"				Ingrese su usuario: ";
			cin>>usuario;
			usuario[h]='.';
			usuario[h+1]='t';
			usuario[h+2]='x';
			usuario[h+3]='t';
			int v=ExisteArchivo(usuario);
			if(v==0){
				cout<<"Ese usuario no existe.";
			}
			else{
				cout<<"				Contrasenia: ";
				cin>>con;
				ifstream ficheroEntrada;
				string frase;
				ficheroEntrada.open (usuario);
				getline(ficheroEntrada, frase);
				ficheroEntrada.close();
				if(frase==con){
					cout<<"Bienvenido Usuario";
				}
				else{
					cout<<"				Contrasenia incorrecta"<<endl<<endl;
					cout<<"				Intentelo de nuevo."<<endl;
					cout<<"				Contrasenia: ";
					cin>>con;
					if(frase==con){
					cout<<"Bienvenido Usuario";
					}
					else{
						cout<<"				Contrasenia incorrecta, el programa volvera al menu principal en 2 segundos, ";
						Sleep(1000);
						cout<<"1 segundo...";
						clean();
						main();
					}
				}
			}
			break;
		}
		case 3: {
			int n;
			cout<<"				Ingrese el numero de caracteres que tiene su nombre de usuario: ";
			cin>>n;
			char us[n+4];
			string pass;
			cout<<"				Escriba un nombre de usuario: ";
			cin>>us;
			us[n]='.';
			us[n+1]='t';
			us[n+2]='x';
			us[n+3]='t';
			ofstream archivo;
			archivo.open(us,ios::out);
			cout<<"				Escriba una Contrasenia para ese usuario: ";
			cin>>pass;
			archivo<<pass<<endl;
			archivo.close();
			cout<<"Bienvenido nuevo usuario";
			Sleep(2000);
			clean();
			cout<<"1. Volver al menu principal."<<endl;
			cout<<"2. Finalizar programa.";
			int y;
			cin>>y;
			if(y==1){
				clean();
			}
			if(y==2){
				fin();
			}
		}
	}
}

void fin(){
	cout<<"El Programa se cerrara 3 segundos";
	Sleep(1000);
	cout<<", 2 segundos, ";
	Sleep(1000);
	cout<<"1 segundo...";
	exit(0);
}

void clean(){
	system("CLS");
}

int ExisteArchivo(char* filename) {
	FILE* f = NULL;
	f = fopen(filename,"r");
	if (f == NULL and errno == ENOENT)
		return 0;
	else {
		fclose(f);
		return 1;
	}
}

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=1024, user-scalable=no">

  <title>Algorirmo de Busqueda de Fibonacci</title>
	
  <!-- Required stylesheet -->
  <link rel="stylesheet" media="screen" href="core/deck.core.css">
  <!--Styles-->
  
  <style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
	vertical-align: baseline;
}
td, th {
    border: 1px solid #dddddd;
    text-align: center;
    padding: 8px;
}
	  th{color: white;
	  }

tr:nth-child(3) {
    background-color: #FF7272;
	color: azure;
	align-content: center;
}
	  tr:nth-child(5) {
    background-color: #FF7272;
	color: white;
	align-content: center;
}
	
</style>
  
  <!-- Extension CSS files go here. Remove or add as needed. -->
  <link rel="stylesheet" media="screen" href="extensions/goto/deck.goto.css">
  <link rel="stylesheet" media="screen" href="extensions/menu/deck.menu.css">
  <link rel="stylesheet" media="screen" href="extensions/navigation/deck.navigation.css">
  <link rel="stylesheet" media="screen" href="extensions/status/deck.status.css">
  <link rel="stylesheet" media="screen" href="extensions/scale/deck.scale.css">

  <!-- Style theme. More available in /themes/style/ or create your own. -->
  <link rel="stylesheet" media="screen" href="themes/style/swiss.css">

  <!-- Transition theme. More available in /themes/transition/ or create your own. -->
  <link rel="stylesheet" media="screen" href="themes/transition/fade.css">

  <!-- Basic black and white print styles -->
  <link rel="stylesheet" media="print" href="core/print.css">

  <!-- Required Modernizr file -->
  <script src="modernizr.custom.js"></script>
</head>
<body>
	<div class="deck-container">

    <!-- Begin slides. Just make elements with a class of slide. -->

    <section class="slide">
      <h1>Técnica de Búsqueda de Fibonacci</h1>
    </section>

    <section class="slide">
      <h2>¿Que és y qué hace?</h2><br>
      <p>Es una técnica basada en la comparación que usa los números de Fibonnacci para la búsqueda de elementos en vectores ordenados usando un algoritmo de tipo divide y vencerás.</p>
      <strong><img src="Images/Fibonacci.png" alt="fibo1" width="500"></strong>
    </section>

    <section class="slide">
      <h2>T.B.F. vs. Búsqueda Binaria</h2>
      <table style="margin-top:250px;">
  <tr bgcolor="#FF0004">
    <th>Técnica de Búsqueda de Fibonacci</th>
    <th>Búsqueda binaria</th>
  </tr>
  <tr>
    <td colspan="2" align="center">Ambos trabajan con vectores ordenados</td>
  </tr>
  <tr>
    <td colspan="2">Tienen complejidad O(log(n))</td>
  </tr>
  <tr>
    <td colspan="2">Son un algortimo divide y conquista</td>
  </tr>
  <tr>
    <td>Divide el vector en partes desiguales</td>
    <td>Divide le vector en partes iguales</td>
  </tr>
  <tr>
    <td>Usa sumas y restas para dividir el rango</td>
    <td>Usa divisiones para dividir el rango</td>
  </tr>
</table>
    </section>
    
    <section class="slide">
    	<h2>Análisis de la complejidad</h2>
    	<p>El peor caso ocurrirá cuando tengamos nuestro objetivo en la parte más larga (2/3) del vector. Es decir, eliminando la fracción más pequeña (1/3) del vector por cada corrida. Así llamamos una vez por n, luego por (2/3)n, luego por (4/9) n y así sucesivamente.
    	<br>Considerando:</p>
    	<img src="Images/formulafibo2.png" alt="formula">
    	<p>Para n ~ c*1.62<sup>n'</sup>  hacemos O(n') comparaciones. Entonces necesitamos O(log(n)) comparaciones.</p>
	</section>
   <section class="slide">
   	<h2>Comparación con la complejidad de O(n)</h2>
   	<img src="images/tabla de comparacion.png" alt="comparacion" width="1600">
   </section>
    <!-- End slides. -->

    <!-- Begin extension snippets. Add or remove as needed. -->

    <!-- deck.navigation snippet -->
    <div aria-role="navigation">
      <a href="#" class="deck-prev-link" title="Previous">&#8592;</a>
      <a href="#" class="deck-next-link" title="Next">&#8594;</a>
    </div>

    <!-- deck.status snippet -->
    <p class="deck-status" aria-role="status">
      <span class="deck-status-current"></span>
      /
      <span class="deck-status-total"></span>
    </p>

    <!-- deck.goto snippet -->
    <form action="." method="get" class="goto-form">
      <label for="goto-slide">Go to slide:</label>
      <input type="text" name="slidenum" id="goto-slide" list="goto-datalist">
      <datalist id="goto-datalist"></datalist>
      <input type="submit" value="Go">
    </form>

    <!-- End extension snippets. -->
<div>
<!-- Required JS files. -->
<script src="jquery.min.js"></script>
<script src="core/deck.core.js"></script>

<!-- Extension JS files. Add or remove as needed. -->
<script src="extensions/menu/deck.menu.js"></script>
<script src="extensions/goto/deck.goto.js"></script>
<script src="extensions/status/deck.status.js"></script>
<script src="extensions/navigation/deck.navigation.js"></script>
<script src="extensions/scale/deck.scale.js"></script>
</div>
<!-- Initialize the deck. You can put this in an external file if desired. -->
<script>
  $(function() {
    $.deck('.slide');
  });
</script>
	  </body>
</html>
